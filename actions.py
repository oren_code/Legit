def validate_title(page, title, element_tag):
    # Wait for navigation to complete
    page.wait_for_load_state('load')
    temp = page.locator(element_tag)
    temp.wait_for(state='visible')
    assert f"{title}" in temp.inner_text(), f'"{title}" Title is missing'


def login(page, username="orensmails@gmail.com", password="N3rUdUzNJjR7XAp!"):
    page.goto("https://main.d2t1pk7fjag8u6.amplifyapp.com/")

    page.fill('[name="username"]', username)
    page.fill('[name="password"]', password)

    submit_button = page.locator('[type="submit"]')
    submit_button.click()

    validate_title(page, "Online Shopping Website", "h1")


def add_different_products(page, amount_of_products):
    add_to_cart_buttons = page.locator('button:has-text("Add to Cart")').all()

    for i in range(amount_of_products):
        add_to_cart_buttons[i].click()


def go_to_shopping_cart(page):
    shopping_cart = page.locator('a:has-text("Shopping Cart")')
    shopping_cart.click()
    validate_title(page, "Shopping Cart", "h2")


def proceed_to_checkout(page):
    proceed_to_checkout_button = page.locator('button:has-text("Proceed to Checkout")')
    proceed_to_checkout_button.click()
    validate_title(page, "Checkout", "h2")


def validate_product_visible(page, name, price, quantity):
    product = page.locator(f'li:has-text("{name} - ${price} (Quantity: {quantity})")')
    assert product.is_visible(), f'Product "{name}" is missing from the shopping cart'


def fill_address(page, address="Fake Address"):
    page.fill('#shipping-address-text', address)


def submit_order(page):
    page.click('#checkout-button:enabled')

    def handle_dialog(dialog):
        dialog.accept()

    page.on('dialog', handle_dialog)


def finalize_order(page, address="Fake Address"):
    fill_address(page, address)
    submit_order(page)

    validate_title(page, "Online Shopping Website", "h1")


def sign_out(page):
    page.click('button[style="float: right;"]')
    page.wait_for_load_state('load')
