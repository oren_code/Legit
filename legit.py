from actions import *


def test_sign_out(page):
    login(page)
    sign_out(page)
    assert page.is_enabled('input[name="username"]'), "'Username' input is not clickable"
    assert page.is_enabled('input[name="password"]'), "'password' input is not clickable"


def test_different_products_order_flow(page):
    login(page)

    add_different_products(page, 2)

    go_to_shopping_cart(page)

    validate_product_visible(page, "Product 1", "20", "1")
    validate_product_visible(page, "Product 2", "30", "1")

    proceed_to_checkout(page)
    finalize_order(page)


def test_entering_only_space_in_address(page):
    login(page)

    add_different_products(page, 1)
    go_to_shopping_cart(page)
    proceed_to_checkout(page)
    fill_address(page, ' ')
    submit_order(page)

    page.wait_for_load_state('load')
    element_count = page.locator('h1:has-text("Online Shopping Website")').count()
    assert element_count == 0, "Should not be redirected to homepage"
