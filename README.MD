### Requirements to run:

Python 3

### Installing dependencies

```
python3 -m pip install -U -r requirements.txt
playwright install
playwright install-deps
```

### Running the tests

```pytest legit.py```