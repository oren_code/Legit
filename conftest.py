import pytest
from playwright.sync_api import sync_playwright


@pytest.fixture
def page():
    # Initialize the browser (you can choose a different browser if you prefer)
    with sync_playwright() as p:
        # Launch the browser
        browser = p.chromium.launch()

        # Create a page within the browser
        page = browser.new_page()

        yield page  # Now, the fixture returns the page, not the browser

        # Teardown: close the browser after the test
        browser.close()
